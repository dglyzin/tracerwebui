(function (ng) {
    "use strict";
    var config = function ($routeProvider, $locationProvider, $httpProvider, JSONEditorProvider, NotificationProvider) {
        $routeProvider
            .when("/", {
                templateUrl: "/static/templates/pages/task-list.html",
                controller: "TasksCtrl",
                controllerAs: "ctrl"
            })
            .when("/archive/", {
                templateUrl: "/static/templates/pages/task-archive.html",
                controller: "TaskArchiveCtrl",
                controllerAs: "ctrl"
            })
            .when("/info/:taskId", {
                templateUrl: "/static/templates/pages/task-tabs.html",
                controller: "TaskInfoCtrl",
                controllerAs: "ctrl"
            })
            .when("/edit/:taskId", {
                templateUrl: "/static/templates/pages/task-tabs.html",
                controller: "TaskEditCtrl",
                controllerAs: "ctrl"
            })
            .when("/cluster/", {
                templateUrl: "/static/templates/pages/cluster-info.html",
                controller: "ClusterInfoCtrl",
                controllerAs: "ctrl"
            })
            .otherwise({
                redirectTo: "/"
            });

        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
        $locationProvider.hashPrefix('!');

        $httpProvider.defaults.headers.common["Accept"] = "application/json";
        $httpProvider.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";
        $httpProvider.defaults.xsrfCookieName = 'csrftoken';
        $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
        $httpProvider.defaults.useXDomain = true;
        $httpProvider.defaults.withCredentials = true;

        // Переопределяем дефолтный transformRequest в $http-сервисе
        $httpProvider.defaults.transformRequest = [function (data) {
            /**
             * рабочая лошадка; преобразует объект в x-www-form-urlencoded строку.
             * @param {Object} obj
             * @return {String}
             */
            var param = function (obj) {
                var query = '';
                var name, value, fullSubName, subName, subValue, innerObj, i;

                for (name in obj) {
                    if (name[0] != "$") {
                        value = obj[name];

                        if (value instanceof Array) {
                            for (i = 0; i < value.length; ++i) {
                                subValue = value[i];
                                fullSubName = name + '[' + i + ']';
                                innerObj = {};
                                innerObj[fullSubName] = subValue;
                                query += param(innerObj) + '&';
                            }
                        }
                        else if (value instanceof Object) {
                            for (subName in value) {
                                subValue = value[subName];
                                fullSubName = name + '[' + subName + ']';
                                innerObj = {};
                                innerObj[fullSubName] = subValue;
                                query += param(innerObj) + '&';
                            }
                        }
                        else if (value !== undefined && value !== null) {
                            query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
                        }
                    }
                }

                return query.length ? query.substr(0, query.length - 1) : query;
            };

            return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
        }];

        JSONEditorProvider.configure({
            defaults: {
                options: {
                    iconlib: 'bootstrap3',
                    theme: 'bootstrap3',
                    disable_edit_json: true,
                    disable_properties: true
                }
            }
        });
        NotificationProvider.setOptions({
            delay: 3000,
            positionX: 'right',
            positionY: 'top'
        });
    };

    ng.module("tracer").config([
        "$routeProvider",
        "$locationProvider",
        "$httpProvider",
        "JSONEditorProvider",
        "NotificationProvider",
        config
    ]);
})(angular);
