(function (ng) {
    'use strict';

    var TaskService = function ($http) {
        return {
            query: function () {
                return $http.get("/api/tasks/");
            },
            create: function (data) {
                return $http.post("/api/tasks/", data);
            },
            get: function (id, serialiser) {
                var ser = serialiser || "short";
                return $http.get("/api/tasks/" + id + "/" + ser + "/");
            },
            update: function (task, serialiser) {
                var ser = serialiser || "short";
                return $http.post("/api/tasks/" + task.id + "/" + ser + "/", task);
            },
            archive: function (id) {
                return $http.delete("/api/tasks/" + id + "/");
            },
            start: function (id) {
                return $http.get("/task/start/" + id + "/");
            },
            stop: function (id) {
                return $http.get("/task/stop/" + id + "/");
            },
            queryArchive: function () {
                return $http.get("/api/tasks/archive/")
            },
            // Archive
            restore: function (id) {
                return $http.post("/api/tasks/archive/" + id + "/");
            },
            delete: function (id) {
                return $http.delete("/api/tasks/archive/" + id + "/");
            }
        };
    };

    ng.module("tracer.task")
        .factory("TaskService", ['$http', TaskService])
})(angular);
