(function(ng) {

    var TASK_STATE = {
        NEW: 0,
        STARTED: 1,
        PREPROCESSING: 2,
        QUEUED: 3,
        RUNNING: 4,
        CANCELLED: 5,
        FINISHED: 6,
        FAILED: 7
    };

    ng.module("tracer.task")
        .constant("taskState", TASK_STATE);
})(angular);
