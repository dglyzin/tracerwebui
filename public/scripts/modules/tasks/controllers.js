(function (ng, $) {
    'use strict';

    var TasksCtrl = function ($scope, $location, $modal, $interval, Notification, taskState, TaskService) {
        var self = this;
        $scope.tasks = [];
        $scope.taskState = taskState;

        this.reload = function () {
            TaskService.query().then(function (response) {
                $scope.tasks = response.data
            });
        };
        this.reload();

        this.onCreate = function () {
            var modalInstance = $modal.open({
                templateUrl: '/static/templates/pages/task-create.html',
                controller: 'TaskCreateCtrl',
                controllerAs: 'ctrl'
            });
            modalInstance.result.then(function () {
                self.reload();
            });
        };

        this.onStop = function (task) {
            task.loked = true;
            task.is_running = false;
            TaskService.update(task).then(function (response) {
                Notification.success("Task stopped");
            }, function (err) {
                self.reload();
                Notification.error("Can't stop task");
            });
        };

        this.onStatus = function (task) {
            task.loked = true;
            var action = task.is_running? TaskService.stop: TaskService.start;
            action(task.id).then(function (response) {
                var notification = response.success? Notification.success: Notification.error;
                notification.call(Notification, response.message);
            }, function (err) {
                self.reload();
                Notification.error("Error!");
            });
        };

        this.onDelete = function (task) {
            return TaskService.archive(task.id).then(function (response) {
                Notification.success("Task has been moved to archive");
                return self.reload();
            }, function (err) {
                Notification.error("Can't move this task to archive");
                return self.reload();
            });
        };

        var interval = $interval(function () {
            TaskService.query().then(function (response) {
                ng.forEach(response.data, function (item) {
                    var index = $scope.tasks.findIndex(function (element) {
                        return item.id == element.id;
                    });
                    if (index >= 0)
                        ng.extend($scope.tasks[index], item);
                    else
                        $scope.tasks.push(item);
                });
            });
        }, 2000);
        $scope.$on('$destroy', function () {
            $interval.cancel(interval);
        });
    };

    var TaskCreateCtrl = function($scope, $modalInstance, Notification, TaskService) {
        $scope.task = {};
        this.onCreate = function() {
            TaskService.create($scope.task).then(function(response) {
                    Notification.success("New task has been created");
                    $modalInstance.close();
            }, function(error) {
                angular.forEach(error.data, function(item, key) {
                    angular.forEach(item, function(item) {
                        $scope.form[key].$setValidity(item, false);
                    });
                });
            });
        };

        this.cancel = function() {
            $modalInstance.dismiss('cancel');
        };
    };

    var TaskTabsCtrl = function($scope, $location, $routeParams) {
        $scope.tabs = [
            {heading: "Info", href: "/info/" + $routeParams.taskId, active: true, tamplateUrl: "/static/templates/pages/task-info.html", controller: "TaskInfoCtrl"},
            {heading: "Edit", href: "/edit/" + $routeParams.taskId, active: false, tamplateUrl: "/static/templates/pages/task-edit.html", controller: "TaskEditCtrl"}
        ];

        $scope.activeTab = function(tab) {
            $location.path(tab.href);
        };

        ng.forEach($scope.tabs, function(item) {
            if ($location.path() == item.href) {
                item.active = true;
                return true;
            }
        })
    };

    var TaskInfoCtrl = function($scope, $routeParams, TaskService) {
        var self = this;
        TaskService.get($routeParams.taskId, "all").then(function (response) {
            $scope.task = response.data;
            var keys = Object.keys(response.data.results.times).sort();
            $scope.activeGallery = keys[0];
        });

        self.setActive = function (key) {
            $scope.activeGallery = key;
        };
    };

    var TaskEditCtrl = function($scope, $http, $routeParams, Notification, TaskService) {

        TaskService.get($routeParams.taskId, "full").then(function(response) {
            $scope.data = JSON.parse(response.data.data);
            $scope.task = response.data;
        });

        $scope.schema = $http.get("/static/schema.json");

        $scope.onUpdate = function () {
            $scope.task.data = JSON.stringify($scope.editor.getValue());
            TaskService.update($scope.task, "full").then(function (response) {
                Notification.success("Task update success");
            }, function (err) {
                Notification.error("Task update error");
            });
        }
    };

    var TaskArchiveCtrl = function ($scope, Notification, TaskService) {
        var self = this;

        this.reload = function () {
            TaskService.queryArchive().then(function (response) {
                $scope.tasks = response.data
            });
        };
        this.reload();

        self.onDelete = function (task) {
            TaskService.delete(task.id).then(function () {
                return self.reload();
            });
        };

        self.onRestore = function (task) {
            TaskService.restore(task.id).then(function () {
               return self.reload();
            });
        }
    };

    ng.module("tracer.task")
        .controller("TasksCtrl", ['$scope', "$location", '$modal', '$interval', "Notification", "taskState", 'TaskService', TasksCtrl])
        .controller("TaskTabsCtrl", ["$scope", "$location", "$routeParams", TaskTabsCtrl])
        .controller("TaskInfoCtrl", ['$scope', "$routeParams", 'TaskService', TaskInfoCtrl])
        .controller("TaskEditCtrl", ['$scope', "$http", "$routeParams", "Notification", 'TaskService', TaskEditCtrl])
        .controller("TaskCreateCtrl", ['$scope', '$modalInstance', 'Notification', 'TaskService', TaskCreateCtrl])
        .controller("TaskArchiveCtrl", ['$scope', 'Notification', 'TaskService', TaskArchiveCtrl]);

})(angular, jQuery);