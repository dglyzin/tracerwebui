(function(ng){
    "use strict";
    ng.module("tracer.task", [
        'ui.bootstrap',
        "ui-notification",
        "angular-json-editor",
        "remoteValidation",
        'bootstrapLightbox'
    ]);
})(angular);
