(function(ng) {
    var ngFile = function() {
        return {
            require: 'ngModel',

            link: function (scope, element, attrs, ngModel) {
                element.bind("change", function (e) {
                    var file = (e.srcElement || e.target).files[0];
                    var reader = new FileReader();
                    reader.onload = function(data) {
                        scope.$apply(function () {
                            ngModel.$setViewValue(data.target.result);
                        });
                    };
                    reader.readAsBinaryString(file);
                });
            }
        }
    };

    var ngGallery = function(Lightbox) {
        return {
            templateUrl: "/static/templates/directives/ng-gallery.html",
            replace: true,
            restrict: "AE",
            scope: {
                images: "="
            },
            controllerAs: "ctrl",
            controller: function ($scope) {
                var self = this;
                self.openLightboxModal = function (index) {
                    Lightbox.openModal($scope.images, index);
                }
            }
        }
    };

    ng.module("tracer.task")
        .directive("ngFile", [ngFile])
        .directive("ngGallery", ['Lightbox', ngGallery]);
})(angular);