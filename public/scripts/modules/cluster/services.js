(function (ng) {
    'use strict';

    var Cluster = function ($http) {
        return {
            info: function () {
                return $http.get("/remote/cluster/info/");
            }
        }
    };

    ng.module("tracer.cluster")
        .factory("ClusterService", ['$http', Cluster]);
})(angular);
