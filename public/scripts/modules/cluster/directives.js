(function(ng) {
    var ngTable = function() {
        return {
            restrict: "AE",
            replace: true,
            templateUrl: '/static/templates/directives/ng-table.html',
            scope: {
                headers: "=",
                data: "=",
                title: "="
            }
        }
    };

    ng.module("tracer.cluster")
        .directive("ngTable", [ngTable]);
})(angular);