(function (ng) {
    'use strict';

    var ClusterInfoCtrl = function ($scope, clusterService) {
        var self = this;
        $scope.sinfo = {};
        $scope.squeue = {};
        $scope.cnode_employment = {};
        self.reload = function () {
            return clusterService.info().then(function (response) {
                $scope.sinfo = response.data.sinfo;
                $scope.squeue = response.data.squeue;
                $scope.partitions = response.data.partitions;
            });
        };

        self.reload();
    };

    ng.module("tracer.cluster")
        .controller("ClusterInfoCtrl", ['$scope', 'ClusterService', ClusterInfoCtrl])

})(angular);