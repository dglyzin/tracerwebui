(function (ng) {
    "use strict";

    ng.module("tracer", ['templates', 'ngRoute', 'tracer.task', 'tracer.cluster']);
})(angular);