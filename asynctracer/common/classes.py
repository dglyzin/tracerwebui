# coding: utf-8

import os


class Cluster(object):
    def __init__(self, user, password, workspace, binfolder, host, port=22):
        self._username = user
        self._password = password
        self._host = host
        self._port = port
        self._workspace = workspace
        self._binfolder = binfolder

    @property
    def connection(self):
        return {
            "host": self._host,
            "port": self._port,
            "password": self._password,
            "username": self._username,
        }

    @property
    def workspace(self):
        return self._workspace

    @property
    def tracer_folder(self):
        return self._binfolder

    def proj_dir(self, task_id):
        return os.path.join(self.workspace, str(task_id))

    def proj_file(self, task_id):
        proj_dir = self.proj_dir(task_id)
        return os.path.join(proj_dir, "project.json")

    def command_jsontobin(self, task_id):
        proj_dir = self.proj_dir(task_id)
        return 'python {0} {1} {2} {3}'.format(
            os.path.join(self.workspace, "hybriddomain/jsontobin.py"),
            os.path.join(proj_dir, "project.json"),
            self.workspace, "-jobId {0}".format(task_id))

    def command_solver(self, task_id):
        return "sh {0}".format(os.path.join(self.workspace, "hybridsolver/bin/HS"))