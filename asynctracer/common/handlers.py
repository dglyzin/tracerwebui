# coding: utf-8

import tornado.web
import tornado.websocket


class DBMixin(object):

    @property
    def db(self):
        return self.application.db


class DBWebSocketHandler(DBMixin, tornado.websocket.WebSocketHandler):
    pass


class DBRequestHandler(DBMixin, tornado.web.RequestHandler):
    pass