# coding: utf-8

import asyncio

from tornado.concurrent import Future


def coroutine(func):
    func = asyncio.coroutine(func)

    def decorator(*args, **kwargs):
        future = Future()

        def future_done(f):
            try:
                future.set_result(f.result())
            except Exception as e:
                future.set_exception(e)

        asyncio.async(func(*args, **kwargs)).add_done_callback(future_done)
        return future
    return decorator
