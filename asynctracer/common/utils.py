# coding: utf-8

import re

parse_configs = lambda key, options: {k.split('_')[1]: options[k] for k in filter(lambda item: item.split('_')[0] == key, options.keys())}


def parse_nodelist(cnodes, state):
    result = {}
    nodes = cnodes[5:]
    try:
        result["cnode{0}".format(int(nodes))] = state
    except Exception as e:
        nodes = cnodes[6:-1].split(",")
        for node in nodes:
            tmp = node.split("-")
            if len(tmp) == 1:
                result["cnode{0}".format(node)] = state
            else:
                for i in range(int(tmp[0]), int(tmp[1]) + 1):
                    result["cnode{0}".format(i)] = state
    return result


def parse_cnode_by_partition(data):
    partitions = {}
    for item in data:
        nodelist = parse_nodelist(item['NODELIST'], item['STATE'])
        partition = item['PARTITION']
        if partition not in partitions:
            partitions[partition] = {}
        partitions[partition].update(nodelist)
    return partitions


def parse_s_table(output):
    ret = {'data': []}
    if not output:
        raise TypeError('Output must not be empty')
    lines = output.split('\n')
    ret['headers'] = lines.pop(0).split()
    for line in lines:
        if not line:
            break
        elements = line.split()
        ret['data'].append({key: elements[i] for i, key in enumerate(ret['headers'])})
    return ret