# coding: utf-8

import asyncio

from sqlalchemy.sql import insert, select, update, delete


class Query():
    table = None
    columns_ = []

    def __init__(self, table):
        self.table = table
        self.columns_ = [i for i in self.table.columns]

    @property
    def columns(self):
        return self.columns_

    def create(self, **kwargs):
        return insert(self.table).values(**kwargs).returning(self.columns_)

    def list(self, **kwargs):
        return select([self.table])

    def get(self, id_, **kwargs):
        return select([self.table]).where(self.table.c.id == id_)

    def update(self, id_, **kwargs):
        return update(self.table).where(self.table.c.id == id_).values(**kwargs).returning(self.columns_)

    def delete(self, id_, **kwargs):
        return delete(self.table).where(self.table.id == id_)


class Service():
    table = None
    query = Query(table)

    def __init__(self, table, connection, query_service=Query):
        self.table = table
        self.query = query_service(table)
        self.conn = connection

    @asyncio.coroutine
    def list(self, **kwargs):
        return (yield from self.conn.execute(self.query.list(**kwargs)))

    @asyncio.coroutine
    def create(self, **kwargs):
        return (yield from self.conn.execute(self.query.create(**kwargs)))

    @asyncio.coroutine
    def get(self, id_):
        task = yield from self.conn.execute(self.query.get(id_=id_))
        task = yield from task.first()
        if task is None:
            raise ValueError("Element by id {0} doesn't exists".format(id_))
        return task

    @asyncio.coroutine
    def update(self, id_, **kwargs):
        task = yield from self.conn.execute(self.query.update(id_=id_, **kwargs))
        task = yield from task.first()
        if task is None:
            raise ValueError("Element by id {0} doesn't exists".format(id_))
        return task

    @asyncio.coroutine
    def delete(self, id_):
        return (yield from self.conn.execute(self.query.delete(id_=id_)))