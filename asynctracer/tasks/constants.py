# coding: utf-8


TASK_STATE_DEFAULT = 0

TASK_STATE = (
    (0, "New"),
    (1, "Started"),
    (2, "Preprocessing"),
    (3, "Queued"),
    (4, "Running"),
    (5, "Cancelled"),
    (6, "Finished"),
    (7, "Failed"),
)