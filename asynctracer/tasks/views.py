# coding: utf-8

import io
import json
import asyncssh
import asyncio

from tornado.gen import Task
from tornado.web import RequestHandler
from tornado.options import options
from tornado.platform.asyncio import to_asyncio_future
from sqlalchemy.sql import update, select

from asynctracer.common.classes import Cluster
from asynctracer.common.handlers import DBRequestHandler
from asynctracer.common.decorators import coroutine
from asynctracer.common.utils import parse_configs, parse_cnode_by_partition, parse_s_table
from asynctracer.tasks.models import tasks


class StartHandler(DBRequestHandler):
    @coroutine
    def get(self, task_id):
        cluster = Cluster(**parse_configs('cluster', options.as_dict()))
        with (yield from self.db) as db_conn:
            task_list = yield from db_conn.execute(update(tasks).where(tasks.c.id == task_id).values(locked=True).returning(tasks.c.id, tasks.c.data))
            task = yield from task_list.first()
            if task is None:
                self.write(json.dumps({"success": False, "message": "Task not exists"}))
                return
            with (yield from asyncssh.connect(**cluster.connection)) as conn:
                proj_dir = cluster.proj_dir(task.id)
                stdin, stdout, stderr = yield from conn.open_session('mkdir -p {0}'.format(proj_dir))
                with (yield from conn.start_sftp_client()) as sftp:
                    with io.StringIO() as src:
                        json.dump(task.data, src)
                        dst_path = cluster.proj_file(task.id)
                        with (yield from sftp.open(dst_path, 'w')) as dst:
                            while True:
                                data = src.read(32768)
                                if not data:
                                    break
                                yield from dst.write(data)

                yield from db_conn.execute(update(tasks).where(tasks.c.id == task_id).values(is_running=True))
                command = cluster.command_jsontobin(task.id)
                stdin, stdout, stderr = yield from conn.open_session(command)
                command = cluster.command_solver(task.id)
                stdin, stdout, stderr = yield from conn.open_session(command)
                yield from db_conn.execute(update(tasks).where(tasks.c.id == task_id).values(locked=False))
                self.write(json.dumps({"success": True, "message": "Task started"}))


class StopHandler(DBRequestHandler):
    @coroutine
    def get(self, task_id):
        cluster = Cluster(**parse_configs('cluster', options.as_dict()))
        with (yield from self.db) as db_conn:
            task_list = yield from db_conn.execute(update(tasks).where(tasks.c.id == task_id).values(locked=True).returning(tasks.c.id, tasks.c.slurm_job))
            task = yield from task_list.first()
            if task is None:
                self.write(json.dumps({"success": False, "message": "Task not exists"}))
                return
            with (yield from asyncssh.connect(**cluster.connection)) as conn:
                stdin, stdout, stderr = yield from conn.open_session('scancel {slurm_job}'.format(task.slurm_job))
                yield from db_conn.execute(update(tasks).where(tasks.c.id == task_id).values(is_running=False, locked=False))
                self.write(json.dumps({"success": True, "message": "Task is stoped now"}))


class ClusterInfoHandler(RequestHandler):

    @asyncio.coroutine
    def execute(self, cluster, command):
        with (yield from asyncssh.connect(**cluster.connection)) as conn:
            stdin, stdout, stderr = yield from conn.open_session(command)
            output = yield from stdout.read()
            info = parse_s_table(output)
        return info

    @coroutine
    def get(self):
        cluster = Cluster(**parse_configs('cluster', options.as_dict()))
        tasks_ = self.execute(cluster, 'sinfo'), self.execute(cluster, 'squeue')
        info, queue = yield from asyncio.gather(*tasks_)
        partitions = parse_cnode_by_partition(info['data'])
        self.write(json.dumps({'sinfo': info, 'squeue': queue, 'partitions': partitions}))


class DownloadHandler(DBRequestHandler):
    CHUNK_SIZE = 512

    @coroutine
    def get(self, task_id):
        with (yield from self.db) as conn:
            task_list = yield from conn.execute(select([tasks]).where(tasks.c.id == task_id))
            task = yield from task_list.first()
            with io.StringIO() as src:
                json.dump(task.data, src)
                self.set_header('Content-Type', 'application/octet-stream')
                self.set_header("Content-Description", "File Transfer")
                self.set_header("Content-Length", src.tell())
                self.set_header('Content-Disposition', 'attachment; filename {}.json'.format(task.title))
                self.flush()
                src.seek(0)
                data = src.read(self.CHUNK_SIZE)
                while data:
                    self.write(data)
                    yield from to_asyncio_future(Task(self.flush))
                    data = src.read(self.CHUNK_SIZE)
            self.finish()