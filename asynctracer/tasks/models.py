# coding: utf-8

import sqlalchemy as sa

from asynctracer.common.fields import JSONType, ChoiceType

from asynctracer.tasks.constants import TASK_STATE, TASK_STATE_DEFAULT

metadata = sa.MetaData()

tasks = sa.Table("tasks", metadata,
                      sa.Column("id", sa.Integer, primary_key=True),
                      sa.Column("title", sa.String(128), index=True, unique=True),
                      sa.Column("data", JSONType),
                      sa.Column("start_date", sa.DateTime, nullable=True),
                      sa.Column("end_date", sa.DateTime, nullable=True),
                      sa.Column("archive", sa.Boolean, default=False),
                      sa.Column("slurm_job", sa.Integer, nullable=True),
                      sa.Column("locked", sa.Boolean, default=False),
                      sa.Column("is_running", sa.Boolean, default=False),
                      sa.Column("readiness", sa.Integer(), default=0),
                      sa.Column("state", ChoiceType(TASK_STATE), default=TASK_STATE_DEFAULT)
                      )


tasks_results = sa.Table("task_results", metadata,
                         sa.Column("id", sa.Integer, primary_key=True),
                         sa.Column("task_id", sa.Integer, sa.ForeignKey(tasks.c.id, onupdate="CASCADE", ondelete="CASCADE")),
                         sa.Column("prob_time", sa.Float, default=None, nullable=True),
                         sa.Column("is_final", sa.Boolean, default=False)
                         )