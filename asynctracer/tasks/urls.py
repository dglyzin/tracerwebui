# coding: utf-8

from asynctracer.tasks.views import StopHandler, StartHandler, DownloadHandler, ClusterInfoHandler

urlpatterns = [
    (r"^/task/stop/(?P<task_id>\d+)/$", StopHandler),
    (r"^/task/start/(?P<task_id>\d+)/$", StartHandler),
    (r"^/task/data/download/(?P<task_id>\d+)/$", DownloadHandler),
    (r"^/remote/cluster/info/$", ClusterInfoHandler)
]