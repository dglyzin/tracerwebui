# coding: utf-8

import asyncio
from sqlalchemy.sql import update

from asynctracer.common.services import Service, Query
from asynctracer.tasks.models import tasks


class TaskQuery(Query):

    def delete(self, id_):
        return update(self.table).where(self.table.c.id == id_).values(archive=True).returning(self.columns)


class TaskService(Service):
    table = tasks
    query = TaskQuery

    def __work_with_lock(self, id_, lock):
        return (yield from self.conn.execute(self.query.update(id_=id_, lock=lock)))

    @asyncio.coroutine
    def lock(self, id_):
        return self.__work_with_lock(id_=id_, lock=True)

    @asyncio.coroutine
    def unlock(self, id_):
        return self.__work_with_lock(id_=id_, lock=False)

    @asyncio.coroutine
    def run(self, id_):
        raise NotImplementedError()

    @asyncio.coroutine
    def stop(self, id_):
        raise NotImplementedError()