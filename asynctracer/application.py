# coding: utf-8

import os
import sys
sys.path.append(os.path.abspath(os.path.dirname(os.path.dirname(__file__))))

import asyncio
import itertools
from aiopg.sa import create_engine

import tornado.web
from tornado.platform.asyncio import AsyncIOMainLoop
from tornado.options import define, parse_config_file, options

from asynctracer.tasks.urls import urlpatterns
from common.utils import parse_configs

BASE_PATH = os.path.dirname(os.path.dirname(__file__))
CONFIG_PATH = os.path.join(BASE_PATH, "configs")

APP_CONFIG_PATH = os.path.join(CONFIG_PATH, "tornado.conf")

# Listen configs
define("listen_port", default="10005", group='listen', type=int)
define("listen_address", default="127.0.0.1", group='listen')
# Application configs
define("app_debug", default=False, group='app', type=bool)
# DB configs
define('db_host', default='localhost', group='db')
define('db_port', default=5432, group='db', type=int)
define('db_user', default='postgres', group='db')
define('db_password', default='postgres', group='db')
define('db_database', default='tracerwebui', group='db')
# Cluster config
define('cluster_host', default='localhost', group='cluster')
define('cluster_port', default=22, group='cluster', type=int)
define('cluster_binfolder', default='/home/dglyzin/Tracer/hybriddomain', group='cluster')
define('cluster_user', default='tester', group='cluster')
define('cluster_password', default='tester', group='cluster')
define('cluster_workspace', default='/home/tester/tracer_workspace', group='cluster')

parse_config_file(APP_CONFIG_PATH)


@asyncio.coroutine
def get_pg_connection():
    return (yield from asyncio.wait_for(create_engine(**parse_configs('db', options.as_dict())), 6000))


if __name__ == "__main__":
    try:
        AsyncIOMainLoop().install()
        ioloop = asyncio.get_event_loop()

        db_conn = ioloop.run_until_complete(get_pg_connection())
        application = tornado.web.Application(itertools.chain(urlpatterns), **parse_configs('app', options.as_dict()))
        application.listen(**parse_configs('listen', options.as_dict()))
        application.db = db_conn

        asyncio.get_event_loop().run_forever()
    except KeyboardInterrupt:
        pass

    asyncio.get_event_loop().close()
