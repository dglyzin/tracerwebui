# Tracer Web UI #

## Запуск проекта на localhost

Проект написан на **Python 2.7**. Так же для сборки проекта потребуется **Nodejs**. Он необходим для сборки клиентской части.

Все дальнейшие дествия предпологают, что вы находитесь в корне проекта.

1. `pip install -r requirements.txt` - установка зависимостей pythoni
2. `sudo npm install` - установка зависимостей nodejs
3. `bower install` - установка клиентских библиотек
4. `gulp build` - сборка клиентской части
5. `cp configs/config.ini.example configs/config.ini && vim configs/config.ini` - копирование настроек проекта и редоктирование.
6. `python manage.py migrate` - выполнение миграций для БД.

Этого достаточно для запуска проекта для разработки.

`python manage.py runserver` - запуск сервер на `localhost`

Также для удобства разработки можно запустить автоматическую сборку клиентской части проект в случае изменения:

`gulp watch`


##docker deploy

1. volume для базы
`docker volume create TracerPostgres`
2. volume для кода
`docker volume create TracerSource`
3. volume для данных пользователя
`docker volume create TracerData`

3. сеть для соединения контейнеров
`docker network create tracer_net`

4. База
`docker run --net=tracer_net --name tracer-postgres -v TracerPostgres:/var/lib/postgresql/data -e POSTGRES_ROOT_PASSWORD=tracer -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=tracer -e POSTGRES_INITDB_ARGS="--encoding=UTF8" -d postgres:9.5`


5. Инициатор для исходников. должен иметь те же юид что и сервер

что делать в инициаторе:
5.1. закачать в `/tracer/source` исходники в ветке `tornado`
5.2. `cp configs/examples/application.conf.example configs/application.conf`
5.3. настроить конфигурацию

`[database]
host=tracer-postgres
port=5432
database=tracer
user=postgres
password=postgres

[cluster]
host=192.168.10.100
port=22
binfolder=/home/dglyzin/Tracer
user=tester
password=??????
workspace=/home/tester/Tracer/user1

[tornado]
host=0.0.0.0
port=11000

[localfs]
workspace=/tracer/data
`

5.4 создаем окружение `virtualenv -p /usr/bin/python3 /tracer/source/venv/webui`
и заполняем его `source /tracer/venv/webui/bin/activate`
`pip install -r requirements.txt`

5.5. `npm install`
5.6. `node_modules/bower/bin/bower --allow-root install`
5.7. `gulp build`

5.8. `python manage.py initdb`

5.9 копируем открытый ключ с `nodezero ( /etc/ssh/ssh_host_rsa_key.pub )`
в папку трейсера `/tracer/source/.ssh/known_hosts`
и показываем на него в `server.py`
в двух местах
`сonn = yield from asyncssh.connect(host = settings.CLUSTER_HOST, known_hosts=(['/tracer/source/.ssh/known_hosts'],[],[] ),  port=settings.CLUSTER_PORT,
                    username=settings.CLUSTER_USER, password=settings.CLUSTER_PASS )`



6. Сервер

`/tracer/source/venv/webui/bin/python3 /tracer/source/tracerwebui/manage.py runserver`

