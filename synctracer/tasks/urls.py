# coding: utf-8

from django.conf.urls import url

from tasks.views import TaskAPIView, TaskUpdateView, TaskUniqueAPIView, TaskArchiveDeleteRestoreView, TaskArchiveView

urlpatterns = [
    # List
    url(r"^api/tasks/$", TaskAPIView.as_view()),
    url(r"^api/tasks/unique/$", TaskUniqueAPIView.as_view()),
    url(r"^api/tasks/(?P<pk>\d+)/$", TaskUpdateView.as_view()),
    url(r"^api/tasks/(?P<pk>\d+)/(?P<serializer>full|short|all)/$", TaskUpdateView.as_view()),
    # Archive
    url(r"^api/tasks/archive/$", TaskArchiveView.as_view()),
    url(r"^api/tasks/archive/(?P<pk>\d+)/$", TaskArchiveDeleteRestoreView.as_view()),
]