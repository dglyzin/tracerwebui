# coding: utf-8

from django.db import models
from django.core.validators import MaxValueValidator
from django.utils.translation import ugettext_lazy as _
from jsonfield import JSONField

from tasks.constants import *


class Tasks(models.Model):

    title = models.CharField(verbose_name=_("Task title"), max_length=128, db_index=True, unique=True)
    # user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_("User"))
    data = JSONField(verbose_name=_("File data"))
    start_date = models.DateTimeField(verbose_name=_("Date start"), null=True, blank=True)
    end_date = models.DateTimeField(verbose_name=_("Date end"), null=True, blank=True)
    archive = models.BooleanField(verbose_name=_("In Archive"), default=False)
    slurm_job = models.IntegerField(verbose_name=_("Slurm job id"), null=True, blank=True)
    state = models.CharField(verbose_name=_("Task state"), choices=TASK_STATE, default=TASK_STATE_DEFAULT, max_length=2)
    locked = models.BooleanField(verbose_name=_("Task is locked"), default=False)
    is_running = models.BooleanField(verbose_name=_("Task status"), default=False)
    readiness = models.PositiveSmallIntegerField(verbose_name=_("Readiness"), default=0, validators=[MaxValueValidator(READINESS_END), ])

    class Meta:
        db_table = "tasks"
        ordering = ("-start_date", "readiness", )


class TaskResult(models.Model):

    task = models.ForeignKey(Tasks, verbose_name=_("Task"), related_name="results")
    picture = models.BinaryField(verbose_name=_("Picture"), null=True, blank=True)
    prob_time = models.FloatField(verbose_name=_("Prob time"), default=None, null=True, blank=True)
    is_final = models.BooleanField(verbose_name=_("Final picture"), default=False)

    class Meta:
        db_table = "task_results"


# class TaskLogs(models.Model):
#
#     task = models.ForeignKey(Tasks, verbose_name=_("Task"))
#     date = models.DateTimeField(verbose_name=_("Date"), auto_now_add=True)
#     log_string = models.TextField(verbose_name=_("Log string"))
#
#     class Meta:
#         db_table = "tracerwebui_task_logs"