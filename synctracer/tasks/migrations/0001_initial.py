# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='TaskLogs',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('date', models.DateTimeField(verbose_name='Date', auto_now_add=True)),
                ('log_string', models.TextField(verbose_name='Log string')),
            ],
            options={
                'db_table': 'tracerwebui_task_logs',
            },
        ),
        migrations.CreateModel(
            name='TaskResult',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('picture', models.BinaryField(null=True, blank=True, verbose_name='Picture')),
                ('prob_time', models.FloatField(null=True, blank=True, verbose_name='Prob time', default=None)),
                ('is_final', models.BooleanField(verbose_name='Final picture', default=False)),
            ],
            options={
                'db_table': 'task_results',
            },
        ),
        migrations.CreateModel(
            name='Tasks',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('title', models.CharField(max_length=128, unique=True, verbose_name='Task title', db_index=True)),
                ('data', jsonfield.fields.JSONField(verbose_name='File data')),
                ('date_start', models.DateTimeField(null=True, blank=True, verbose_name='Date start')),
                ('date_end', models.DateTimeField(null=True, blank=True, verbose_name='Date end')),
                ('archive', models.BooleanField(verbose_name='In Archive', default=False)),
                ('slurm_job', models.IntegerField(null=True, blank=True, verbose_name='Slurm job id')),
                ('state', models.IntegerField(choices=[(0, 'New'), (1, 'Started'), (2, 'Preprocessing'), (3, 'Queued'), (4, 'Running'), (5, 'Cancelled'), (6, 'Finished'), (7, 'Failed')], verbose_name='Task state', default=0)),
                ('locked', models.BooleanField(verbose_name='Task is locked', default=False)),
                ('is_running', models.BooleanField(verbose_name='Task status', default=False)),
                ('readiness', models.PositiveSmallIntegerField(validators=[django.core.validators.MaxValueValidator(100)], verbose_name='Readiness', default=0)),
            ],
            options={
                'ordering': ('-date_start', 'readiness'),
                'db_table': 'tasks',
            },
        ),
        migrations.AddField(
            model_name='taskresult',
            name='task',
            field=models.ForeignKey(to='tasks.Tasks', verbose_name='Task', related_name='results'),
        ),
        migrations.AddField(
            model_name='tasklogs',
            name='task',
            field=models.ForeignKey(to='tasks.Tasks', verbose_name='Task'),
        ),
    ]
