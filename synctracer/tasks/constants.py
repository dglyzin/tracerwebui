# coding: utf-8

from django.utils.translation import ugettext as _

TASK_STATE_DEFAULT = 0

TASK_STATE = (
    ('0', _("New")),
    ('1', _("Started")),
    ('2', _("Preprocessing")),
    ('3', _("Queued")),
    ('4', _("Running")),
    ('5', _("Cancelled")),
    ('6', _("Finished")),
    ('7', _("Failed")),
)

READINESS_END = 100