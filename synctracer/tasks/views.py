# coding: utf-8

from rest_framework.response import Response
from rest_framework import generics, status

from tasks.models import Tasks
from tasks.serializers import TaskSerializer, TaskFullSerializer, TaskWithResultSerializer


class TaskAPIView(generics.ListCreateAPIView):
    queryset = Tasks.objects.filter(archive=False)
    serializer_class = {"GET": TaskSerializer, "POST": TaskFullSerializer}

    def get_serializer_class(self):
        return self.serializer_class[self.request.method]


class TaskUniqueAPIView(generics.GenericAPIView):

    def get(self, request, *args, **kwargs):
        value = request.query_params.get("value", "")
        task_exists = Tasks.objects.filter(title=value).exists()
        if task_exists:
            response = Response({"isValid": False, "value": value}, status=status.HTTP_200_OK)
        else:
            response = Response({"isValid": True, "value": value}, status=status.HTTP_200_OK)
        return response


class TaskUpdateView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Tasks.objects.all()
    serializer_class = {"full": TaskFullSerializer, "short": TaskSerializer, "all": TaskWithResultSerializer}

    def get_serializer_class(self):
        ser_name = self.kwargs.get('serializer', 'short')
        return self.serializer_class[ser_name]

    def post(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def perform_destroy(self, instance):
        instance.is_running = False
        instance.locked = False
        instance.archive = True
        instance.save()


class TaskArchiveView(generics.ListAPIView):
    queryset = Tasks.objects.filter(archive=True)
    serializer_class = TaskSerializer
    http_method_names = ("get", )


class TaskArchiveDeleteRestoreView(generics.DestroyAPIView):
    queryset = Tasks.objects.filter(archive=True)
    serializer_class = TaskSerializer
    http_method_names = ('post', 'delete')

    def post(self, request, *args, **kwargs):
        instance = self.get_object()
        instance.is_running = False
        instance.locked = False
        instance.archive = False
        instance.save()
        serializer = self.get_serializer(instance)
        return Response(serializer.data, status=status.HTTP_200_OK)