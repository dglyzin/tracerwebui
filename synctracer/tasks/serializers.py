# coding: utf-8

from rest_framework.serializers import ModelSerializer, SerializerMethodField, CharField
from rest_framework.validators import UniqueValidator

from tasks.models import Tasks
from common.fields import JSONSerializerField


class TaskSerializer(ModelSerializer):
    title = CharField(required=True, validators=[UniqueValidator(Tasks.objects.all(), message="unique")])
    state = SerializerMethodField()

    def get_state(self, obj):
        return {"code": obj.state, "display": obj.get_state_display()}

    class Meta:
        model = Tasks
        exclude = ("data", "slurm_job", )


class TaskFullSerializer(TaskSerializer):
    data = JSONSerializerField()

    class Meta:
        model = Tasks
        exclude = ("slurm_job", )


class TaskWithResultSerializer(ModelSerializer):
    results = SerializerMethodField()
    state = SerializerMethodField()

    def get_results(self, obj):
        queryset = obj.results.all().values("id", "task__id", "prob_time", "is_final")
        items = {}
        times = []
        for item in queryset:
            item['url'] = "/task/image/{0}/{1}/".format(item['task__id'], item['id'])
            key = str(item['prob_time'])
            if key not in items:
                times.append(item['prob_time'])
                items[key] = []
            items[key].append(item)
        return {'items': items, "times": map(lambda i: str(i), sorted(times))}

    def get_state(self, obj):
        return {"code": obj.state, "display": obj.get_state_display()}

    class Meta:
        model = Tasks
        exclude = ("slurm_job", 'data')