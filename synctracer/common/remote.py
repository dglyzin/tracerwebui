# # -*- coding: utf-8 -*-
#
# import os
# import json
# import cStringIO
# import socket
# import paramiko
#
# from common.classes import ClusterConnection
#
# remoteRunScriptName = 'project.sh'
# remoteProjectFileName = 'project.json'
# remoteMp4Name = 'project.mp4'
#
# cluster = ClusterConnection("tester", "tester", "/home/tester/tracer_workspace")
#
#
# def project_run(task, continueEnabled, continueFnameProvided, continueFileName, debug=False):
#     '''
#       task:  task from database
#       continueEnabled: true if user wants to continue from computed file
#       continueFnameProvided: true if user wants to continue from specific file, false if the last computed file to be used
#       continueFileName:
#       debug: true if user wants to run small problem (10 min. max)
#     '''
#
#     # prepare command line argumetnts for preprocessor
#     optionalArgs = " -jobId {0}".format(task.id)
#     # if continueEnabled:
#     #     optionalArgs += " -cont"
#     #     if continueFnameProvided:
#     #         optionalArgs += " "+continueFileName
#     if debug:
#         optionalArgs += " -debug"
#
#     # get project file name without extension
#     client = paramiko.SSHClient()
#     client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
#     try:
#         client.connect(**cluster.connection)
#
#         print "Checking if folder {0} exists...".format(cluster.workspace)
#         stdin, stdout, stderr = client.exec_command('test -d {0}'.format(cluster.workspace))
#         if stdout.channel.recv_exit_status():
#             raise Exception("Please create workspace folder and put hybriddomain preprocessor into it")
#         else:
#             print "Workspace OK."
#
#         proj_folder = os.path.join(cluster.workspace, task.id)
#         print "Creating/cleaning project folder: "
#         stdin, stdout, stderr = client.exec_command('test -d {0}'.format(proj_folder))
#         if stdout.channel.recv_exit_status():
#             stdin, stdout, stderr = client.exec_command('mkdir {0}'.format(proj_folder))
#             print "Folder created."
#         else:
#             if not continueEnabled:
#                 stdin, stdout, stderr = client.exec_command('rm -rf {0}/*'.format(proj_folder))
#                 stdout.read()
#                 print "Folder cleaned."
#             else:
#                 print "Folder exists, no cleaning needed."
#                 # now check if file to continue from exists
#                 if continueFnameProvided:
#                     print "Checking if file to continue from  ({0}) exists...".format(continueFileName)
#                     stdin, stdout, stderr = client.exec_command('test -f {0}'.format(continueFileName))
#                     if stdout.channel.recv_exit_status():
#                         raise Exception("File not found, please specify existing file to continue")
#                     else:
#                         print "File OK."
#
#         buffer_ = cStringIO.StringIO()
#         json.dump(task.data, buffer_)
#         buffer_.seek(0)
#         sftp = client.open_sftp()
#         sftp.putfo(buffer_, os.path.join(proj_folder, remoteProjectFileName))
#         sftp.close()
#         buffer_.close()
#
#         # 3 Run jsontobin on json
#         print '\nRunning preprocessor:'
#         command = 'python {0} {1} {2} {3}'.format(
#             os.path.join(cluster.tracer_folder, "hybriddomain/jsontobin.py"),
#             os.path.join(proj_folder, remoteProjectFileName),
#             cluster.tracer_folder,
#             optionalArgs
#         )
#
#         stdin, stdout, stderr = client.exec_command(command)
#         print stdout.read()
#         print "jsontobin stderr:"
#         print stderr.read()
#         print "stderr END"
#         # 4 Run Solver binary on created files
#         solver_executable = os.path.join(cluster.tracer_folder, "hybridsolver/bin/HS")
#         print "Checking if solver executable at {0} exists...".format(solver_executable)
#         stdin, stdout, stderr = client.exec_command('test -f {0}'.format(solver_executable))
#         if stdout.channel.recv_exit_status():
#             print "Please provide correct path to the solver executable."
#             return
#         else:
#             print "Solver executable found."
#
#         stdin, stdout, stderr = client.exec_command('sh {0}'.format(os.path.join(proj_folder, remoteRunScriptName) ))
#         print stdout.read()
#         print stderr.read()
#
#         # get resulting files
#         # sftp = client.open_sftp()
#         # sftp.get(projFolder+"/"+remoteMp4Name, projectPathName+".mp4")
#         # s1ftp.close()
#
#         client.close()
#     # Обрабатываю исключения
#     except paramiko.AuthenticationException:
#         return u'Неверный логин или пароль'
#     except socket.error:
#         return u'Указан неправильный адрес или порт'
#     except paramiko.SSHException:
#         return u'Ошибка в протоколе SSH'
#     except Exception as e:
#         print e
#         return e
#
#     @app.task(ignore_result=True, queue="tasks")
# def stop_task(task_id):
#     task = Tasks.objects.get(id=task_id)
#     client = paramiko.SSHClient()
#     client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
#     cluster = ClusterConnection("tester", "tester", "/home/tester/tracer_workspace")
#     try:
#         client.connect(**cluster.connection)
#         stdin, stdout, stderr = client.exec_command("scancel {slurm_job}".format(slurm_job=task.slurm_job))
#         print stdout.read()
#         print stderr.read()
#         client.close()
#         task.is_running = False
#         task.V = False
#         task.save()
#     except Exception as e:
#         print e
#         pass
