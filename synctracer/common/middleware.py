# coding: utf-8

from django.conf import settings
from django.utils import translation


class LocaleMiddleware(object):

    def process_request(self, request):
        language = request.user.language if request.user.is_authenticated() else settings.LANGUAGE_CODE
        translation.activate(language)
        request.LANGUAGE_CODE = translation.get_language()