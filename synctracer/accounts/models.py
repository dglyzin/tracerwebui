# coding: utf-8

import random
import hashlib

from django.db import models
from django.core.mail import EmailMultiAlternatives
from django.conf import settings
from django.utils import timezone, six
from django.contrib.auth import models as auth_model
from django.template.loader import render_to_string
from django.utils.translation import ugettext_lazy as _
from django.utils.translation import ugettext as __


def generate_activation_key(id_):
    salt = hashlib.sha1(six.text_type(random.random()).encode("ascii")).hexdigest()[:6]
    salt = salt.encode("ascii")
    id_str = str(id_)
    if isinstance(id_str, six.text_type):
        id_str = id_str.encode("utf-8")
    activation_key = hashlib.sha1(id_str+salt).hexdigest()
    return activation_key


class UserManager(auth_model.UserManager):

    def _create_user(self, email, password, is_staff, is_active, **extra_fields):
        """
        Creates and saves a User with the given username, email and password.
        """
        now = timezone.now()
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, is_staff=is_staff, is_active=is_active, date_joined=now, **extra_fields)
        user.activation_key = generate_activation_key(email)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        return self._create_user(email, password, False, False, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        return self._create_user(email, password, True, True, **extra_fields)

    def send_activation_email(self, user, host, send=True):
        kw = {"user": user, "host": host}
        message_txt = render_to_string("emails/activation_email.txt", kw)
        message_html = render_to_string("emails/activation_email.html", kw)
        email_message = EmailMultiAlternatives(__("Confirm email address, TracerWebUI"), message_txt, to=[user.email])
        email_message.attach_alternative(message_html, "text/html")
        if send:
            email_message.send()
        return email_message

    def activate_user(self, activation_key):
        user = User.objects.get(activation_key=activation_key)
        user.is_active = True
        user.save()
        return user


class User(auth_model.AbstractBaseUser):
    email = models.EmailField(verbose_name=_('Email'), unique=True)
    is_staff = models.BooleanField(verbose_name=_('Staff status'), default=False, help_text=_('Designates whether the user can log into this admin site.'))
    is_active = models.BooleanField(verbose_name=_('Active'), default=False, help_text=_('Designates whether this user should be treated as active. Unselect this instead of deleting accounts.'))
    date_joined = models.DateTimeField(verbose_name=_('Date joined'), auto_now_add=True)
    activation_key = models.CharField(verbose_name=_("Activation key"), max_length=40, unique=True)
    language = models.CharField(verbose_name=_("Interface language"), choices=settings.LANGUAGES, default="en", max_length=2)
    # cluster_username = EncryptedCharField(verbose_name=_('Cluster username'), max_length=128, null=True, blank=True)
    # cluster_password = EncryptedCharField(verbose_name=_('Cluster password'), max_length=128, null=True, blank=True)
    cluster_workspace = models.CharField(verbose_name=_('Cluster workspace'), max_length=256, null=True, blank=True)

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    objects = UserManager()

    def get_full_name(self):
        return u"{}".format(self.email)

    def get_short_name(self):
        return u"{}".format(self.email)

    def has_perm(self, *args, **kwargs):
        return True

    def has_module_perms(self, *args, **kwargs):
        return True

    def connection_to_klaster(self):
        return ClusterConnection("tester", "tester")

    class Meta:
        db_table = "tracerwebui_users"
