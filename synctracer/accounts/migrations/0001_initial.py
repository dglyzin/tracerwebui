# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import accounts.models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(null=True, blank=True, verbose_name='last login')),
                ('email', models.EmailField(max_length=254, unique=True, verbose_name='Email')),
                ('is_staff', models.BooleanField(help_text='Designates whether the user can log into this admin site.', verbose_name='Staff status', default=False)),
                ('is_active', models.BooleanField(help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.', verbose_name='Active', default=False)),
                ('date_joined', models.DateTimeField(verbose_name='Date joined', auto_now_add=True)),
                ('activation_key', models.CharField(max_length=40, unique=True, verbose_name='Activation key')),
                ('language', models.CharField(max_length=2, choices=[('ru', 'Russian'), ('en', 'English')], verbose_name='Interface language', default='en')),
                ('cluster_workspace', models.CharField(max_length=256, null=True, blank=True, verbose_name='Cluster workspace')),
            ],
            options={
                'db_table': 'tracerwebui_users',
            },
            managers=[
                ('objects', accounts.models.UserManager()),
            ],
        ),
    ]
