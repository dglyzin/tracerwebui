"""
Django settings for tracerwebui project.

Generated by 'django-admin startproject' using Django 1.8.3.

For more information on this file, see
https://docs.djangoproject.com/en/1.8/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.8/ref/settings/
"""

import os
from configparser import RawConfigParser

from django.contrib import messages
from django.utils.translation import ugettext_lazy as _

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
CONFIG_DIR = os.path.abspath(os.path.join(BASE_DIR, "configs"))

config = RawConfigParser()
config.read(os.path.join(CONFIG_DIR, 'django.ini'))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.8/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'ndpi@@rt7ug8#kdvu&8trtxok#*m7-(=_1yilky!m3-7)2!c%s'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ADMINS = (
    ('Krivosheev', 'py.krivosheev@gmail.com'),
)
ALLOWED_HOSTS = ["*", ]


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Requirements
    'rest_framework',
    # Apps
    'accounts',
    'tasks',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'common.middleware.LocaleMiddleware',
)

ROOT_URLCONF = 'synctracer.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, "synctracer/templates")
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'synctracer.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.8/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': config.get('database', 'ENGINE'),
        'HOST': config.get('database', 'HOST'),
        'NAME': config.get('database', 'NAME'),
        'USER': config.get('database', 'USER'),
        'PASSWORD': config.get('database', 'PASSWORD'),
        'PORT': config.get('database', 'PORT')
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.8/topics/i18n/

LANGUAGE_CODE = 'ru'
LANGUAGES = (
    ("ru", _("Russian")),
    ("en", _("English")),
)

LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'locale'),
)

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.8/howto/static-files/

STATIC_URL = '/static/'
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "public", 'vendor'),
    os.path.join(BASE_DIR, "public", 'build'),
    os.path.join(BASE_DIR, "public", 'schema'),
)

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

STATIC_ROOT = os.path.join(BASE_DIR, 'static')

if DEBUG:
    SCHEME_FILE = os.path.join(BASE_DIR, "public", "schema", "schema.json")
else:
    SCHEME_FILE = os.path.join(STATIC_ROOT, "schema", "schema.json")

# Message config
MESSAGE_TAGS = {
    messages.ERROR: 'danger'
}


# Email settings
EMAIL_BACKEND = config.get("email", "BACKEND")
EMAIL_HOST = config.get("email", "HOST")
EMAIL_PORT = config.get("email", "PORT")
EMAIL_HOST_USER = config.get("email", "HOST_USER")
EMAIL_HOST_PASSWORD = config.get("email", "HOST_PASSWORD")
EMAIL_USE_TLS = config.getboolean("email", "USE_TLS")
DEFAULT_FROM_EMAIL = config.get("email", "DEFAULT_FROM_EMAIL")

# Auth
AUTH_USER_MODEL = "accounts.User"

# Rest Framework
REST_FRAMEWORK = {
    "DATETIME_FORMAT": "%H:%M %d.%m.%Y",
}

APPEND_SLASH = True
PREPEND_WWW = False
