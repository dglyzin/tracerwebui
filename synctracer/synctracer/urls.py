"""tracerwebui URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf import settings
from django.contrib import admin
from django.conf.urls.static import static
from django.conf.urls import include, url

from synctracer.views import Index


urlpatterns = [
    url("^$", Index.as_view(), name="index"),
    url("^cluster/$", Index.as_view(), name="index"),
    url("^edit/(\d+)$", Index.as_view()),
    url("^info/(\d+)$", Index.as_view()),
    url("^archive/$", Index.as_view()),
    # Tasks
    url(r'^', include('tasks.urls', namespace='tasks')),
    # Accounts
    # url(r'^api/account', include('accounts.urls', namespace="account")),

    url(r'^admin/', include(admin.site.urls)),
]

# ... your normal urlpatterns here

if settings.DEBUG:
    # static files (images, css, javascript, etc.)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)