'use strict';

var gulp = require('gulp');
var templateCache = require('gulp-angular-templatecache');
var concat = require('gulp-concat');


gulp.task('style', function() {
    gulp.src(['public/css/*.css'])
        .pipe(gulp.dest("public/build/css"));
});

gulp.task('js', function() {
    gulp.src([
        "public/scripts/app.js",
        "public/scripts/configs.js",
        "public/scripts/modules/**/_index.js",
        "public/scripts/modules/**/*.js"])
        .pipe(concat('app.min.js'))
        .pipe(gulp.dest('public/build'));
});

gulp.task('templates', function () {
    gulp.src(['public/templates/**/*.html'])
        .pipe(templateCache({standalone: true, root: '/static/templates/'}))
        .pipe(gulp.dest('public/build'));
});

gulp.task('watch', function () {
    gulp.watch(['public/css/*.css'], {
        interval: 500
    }, ['style']);
    gulp.watch(["public/scripts/**/*.js"], {
        interval: 500
    }, ['js']);
    gulp.watch(['public/templates/**/*.html'], {
        interval: 500
    }, ['templates'])
});

gulp.task("build", ['js', 'style', 'templates']);